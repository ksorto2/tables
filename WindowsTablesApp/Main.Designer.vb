﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.TablesListView = New System.Windows.Forms.ListView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TablesImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.SuspendLayout()
        '
        'TablesListView
        '
        Me.TablesListView.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.TablesListView.LargeImageList = Me.TablesImageList
        Me.TablesListView.Location = New System.Drawing.Point(31, 69)
        Me.TablesListView.Name = "TablesListView"
        Me.TablesListView.Size = New System.Drawing.Size(691, 288)
        Me.TablesListView.TabIndex = 0
        Me.TablesListView.UseCompatibleStateImageBehavior = False
        Me.TablesListView.View = System.Windows.Forms.View.Tile
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(26, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 25)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Mesas"
        '
        'TablesImageList
        '
        Me.TablesImageList.ImageStream = CType(resources.GetObject("TablesImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.TablesImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.TablesImageList.Images.SetKeyName(0, "disponible.png")
        Me.TablesImageList.Images.SetKeyName(1, "reservada.png")
        Me.TablesImageList.Images.SetKeyName(2, "ocupada.png")
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(755, 471)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TablesListView)
        Me.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(771, 510)
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Mesas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TablesListView As ListView
    Friend WithEvents Label1 As Label
    Friend WithEvents TablesImageList As ImageList
End Class
