﻿Public Class Main
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        DisplayTables()
    End Sub

    Private Sub DisplayTables()
        Dim tables As List(Of Mesa) = GetTables()
        Dim listviewItems As List(Of ListViewItem) = New List(Of ListViewItem)
        For Each table As Mesa In tables
            Dim imageIndex = CType(table.Estado, Integer)
            listviewItems.Add(New ListViewItem(GetStringState(table.Estado), imageIndex))
        Next
        TablesListView.Items.AddRange(listviewItems.ToArray())
    End Sub


    Function GetStringState(ByVal state As Estados) As String
        Select Case state
            Case Estados.Disponible
                Return "Disponible"
            Case Estados.Reservada
                Return "Reservada"
            Case Estados.Ocupada
                Return "Ocupada"
            Case Else
                Return "No Disponible"
        End Select
    End Function
    Function GetTables()
        Return New List(Of Mesa) From
            {
             New Mesa(Estados.Disponible, 1),
             New Mesa(Estados.Disponible, 2),
             New Mesa(Estados.Reservada, 3),
             New Mesa(Estados.Reservada, 4),
             New Mesa(Estados.Disponible, 5),
             New Mesa(Estados.Disponible, 6),
             New Mesa(Estados.Disponible, 7),
             New Mesa(Estados.Ocupada, 8),
             New Mesa(Estados.Disponible, 9),
             New Mesa(Estados.Disponible, 10),
             New Mesa(Estados.Ocupada, 11)
            }
    End Function
End Class
